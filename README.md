# flow-schemer

Gulp plugin for generating schemas for UI validations, DTO (data transfer objects) validations and database models from a common definition based on object models. 

Currently, flow-schemer can generate [JOI](https://www.npmjs.com/package/@hapi/joi) schemas for request body validation and [YUP](https://www.npmjs.com/package/yup) schemas for UI forms validations.

flow-schemer uses babylon parser to find out the relationship between your models defined used flow interfaces. Schema generation is aided by annotating your models with metadata comments, an annotation is of the form ```@<annotation_name>(<annotation_value>)```, where  ```annotation_value``` is a
JSON object literal with the following additions:

* keys must be unquoted
* use of regular expression literals
* use of constants as values
* "existential properties": a kind of syntactic sugar for representing an truthy value meaning "object is" or "object has property"

Example:

```
// @flow
interface OrderDetail {
    product: Product,
    /**
        quantity must be a positive integer and less than a predefined
        max_stock_existences.
        @property({
            subtype: "integer",
            positive,
            max: max_stock_existences,
        })
    */
    quantity: number,
}
```

### Running example

This projects uses nvm for managing node versions, so you may need to install it.
```
$ nvm install
$ nvm use
$ yarn # (or npm install)
$ cd examples
$ npx gulp
```
