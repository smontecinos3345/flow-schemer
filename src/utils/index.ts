// @flow
import { isPrimitive } from "util";

export function lcfirst(str: string) {
  return str.charAt(0).toLowerCase() + str.substring(1);
}

export function merge(args: any[]) {
  const res: any = {};
  for (const obj of args) {
    for (const key of Object.keys(obj)) {
      res[key] = obj[key];
    }
  }
  return res;
}

export function prettyPrint(o: any, indent: number = 0): string {
  let out = "";

  if (o instanceof Array) {
    return o.map((x) => prettyPrint(x, indent + 1)).join(", ");
  } else if (o.prettyPrint !== undefined) {
    return o.prettyPrint(indent + 1);
  } else if (isPrimitive(o) || o instanceof RegExp) {
    return o.toString();
  } else {
    for (const k of Object.keys(o)) {
      const val = o[k];
      out += new Array(4 * indent + 1).join(" ") + k + ": ";
      if (typeof val === "object") {
        if (val.prettyPrint === undefined) {
          out += "{\n" + prettyPrint(val, indent + 1) + new Array(4 * indent + 1).join(" ") + "}";
        } else {
          out += val.prettyPrint(indent + 1);
        }
      } else if (typeof val === "function") {
        continue;
      } else {
        out += "\"" + val + "\"";
      }
      out += ",\n";
    }
    return `{\n${out}\n}`;
  }
}
export * from "./types";
