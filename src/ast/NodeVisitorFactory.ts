import { DefaultNodeVisitor } from "./DefaultNodeVisitor";
import { InterfaceNodeVisitor } from "./InterfaceNodeVisitor";
import { NodeVisitor } from "./NodeVisitor";
import { PropertyNodeVisitor } from "./PropertyNodeVisitor";
import { TypeNodeVisitor } from "./TypeNodeVisitor";

export class NodeVisitorFactory {
  public static readonly TYPE_NODE_VISITOR = "type";
  public static readonly INITIAL_NODE_VISITOR = "program";
  public static readonly INTERFACE_NODE_VISITOR = "interface";
  public static readonly PROPERTY_NODE_VISITOR = "property";

  public getNodeVisitor(type: string, node: any, context: any): NodeVisitor {

    switch (type) {
      case NodeVisitorFactory.INTERFACE_NODE_VISITOR:
        return new InterfaceNodeVisitor(this, context, node);
      case NodeVisitorFactory.TYPE_NODE_VISITOR:
        return new TypeNodeVisitor(this, context, node);
      case NodeVisitorFactory.INITIAL_NODE_VISITOR:
        return new DefaultNodeVisitor(this, context, node);
      case NodeVisitorFactory.PROPERTY_NODE_VISITOR:
        return new PropertyNodeVisitor(this, context, node);
      default:
        throw new Error(`Unknown visitor type ${type}`);
    }
  }

}
