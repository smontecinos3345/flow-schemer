import { NodeVisitorFactory } from "./NodeVisitorFactory";
import { SchemaBodyNodeVisitor } from "./SchemaBody";

export class InterfaceNodeVisitor extends SchemaBodyNodeVisitor {

  constructor(factory: NodeVisitorFactory, context: any, ast: any) {
    super("entity", factory, context, ast);
  }

  public getBodyNode() {
    return this.ast.body.properties;
  }

}
