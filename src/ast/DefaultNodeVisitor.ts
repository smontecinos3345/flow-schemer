import { tokenizer } from "../schema";
import { MetadataParser } from "../schema/metadata-parser/MetadataParser";
import { NodeVisitor } from "./NodeVisitor";
import { NodeVisitorFactory } from "./NodeVisitorFactory";

export class DefaultNodeVisitor implements NodeVisitor {

  constructor(
    protected fact: NodeVisitorFactory,
    protected context: any,
    protected ast: any,
  ) {
  }

  public visit() {
    const self: any = this;
    let stack;
    let parent;
    let keys: string[] = [];
    let index = -1;

    do {
      index++;
      if (stack && index === keys.length) {
        parent = stack.parent;
        keys = stack.keys;
        index = stack.index;
        stack = stack.prev;
      } else {
        const node: any = parent ? parent[keys[index]] : this.getBodyNode();

        if (node && typeof node === "object" && (node.type || node.length)) {
          if (node.type) {
            let visitorFnName = `visit${node.type}`;
            visitorFnName = visitorFnName in self ? visitorFnName : "defaultVisit";
            if ((self as any)[visitorFnName](node, this.context) === false) {
              continue;
            }
          }
          stack = { parent, keys, index, prev: stack };
          parent = node;
          keys = Object.keys(node);
          index = -1;
        }
      }
    } while (stack);
  }

  public visitInterfaceDeclaration(node: any) {
    this.fact
      .getNodeVisitor(NodeVisitorFactory.INTERFACE_NODE_VISITOR, node, this.context)
      .visit();
    return false;
  }

  public visitTypeAlias(node: any) {
    this.fact
      .getNodeVisitor(NodeVisitorFactory.TYPE_NODE_VISITOR, node, this.context)
      .visit();
    return false;
  }

  public defaultVisit(node: any) {
    return true;
  }

  public getBodyNode() {
    return this.ast.program;
  }

  protected getMetadata(node: any) {
    const res = [];
    if (node && node.leadingComments) {
      for (const comment of node.leadingComments) {
        const it = tokenizer.tokenize(comment.value);
        const parser = new MetadataParser(it, this.context.builder.symbols);
        res.push(parser.parse());
      }
    }
    return res;
  }

}
