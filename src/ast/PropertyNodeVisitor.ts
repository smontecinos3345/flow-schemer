import { ArrayTypeSchema } from "../schema";
import { PrimitiveTypeSchema, TypeSchema } from "../schema";
import { DefaultNodeVisitor } from "./DefaultNodeVisitor";
import { NodeVisitorFactory } from "./NodeVisitorFactory";

export class PropertyNodeVisitor extends DefaultNodeVisitor {

  protected entity: any;

  constructor(
    factory: NodeVisitorFactory,
    context: any,
    ast: any,
  ) {
    super(factory, context, ast);
  }

  public visit() {
    const { context, ast } = this;
    const metadata = this.getMetadata(ast);
    const type = this.extractType(context, ast.value);
    context.addProperty(ast.key.name, ast.optional ? type.asNullable() : type, metadata);
    return super.visit();
  }

  public extractType(context: any, node: any) {
    let type: string;
    let arr: RegExpMatchArray | null;
    // tslint:disable-next-line: no-conditional-assignment
    if ((type = node.type) === "GenericTypeAnnotation") {
      const name = node.id.name;
      return name === "Date" ? PrimitiveTypeSchema.date() : this.complexType(name, context, node);
// tslint:disable-next-line: no-conditional-assignment
    } else if ((arr = /^(.+)TypeAnnotation$/.exec(type)) != null) {
      const primitiveTable = (PrimitiveTypeSchema as any);
      const key = arr[1][0].toLowerCase() + arr[1].substr(1);
      return primitiveTable[key]();
    } else {
      throw new Error("Uknown type annotation");
    }
  }

  public complexType(name: string, context: any, node: any): TypeSchema {
    switch (name) {
      case "Array":
        return new ArrayTypeSchema(this.extractType(context, node.typeParameters.params[0]));
      default:
        return context.createSchemaReference(name);
    }
  }

}
