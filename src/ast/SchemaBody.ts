import { DefaultNodeVisitor } from "./DefaultNodeVisitor";
import { NodeVisitorFactory } from "./NodeVisitorFactory";

export class SchemaBodyNodeVisitor extends DefaultNodeVisitor {

  protected entity: any;

  constructor(
    protected type: string,
    factory: NodeVisitorFactory,
    context: any,
    ast: any,
  ) {
    super(factory, context, ast);
  }

  public visit() {
    this.entity = this.context.createEntity(this.ast.id.name,
      this.getMetadata(this.ast), this.ast.extends.map((x: any) => x.id.name));
    return super.visit();
  }

  public visitObjectTypeProperty(node: any) {
    const visitor = this.fact.getNodeVisitor(NodeVisitorFactory.PROPERTY_NODE_VISITOR, node, this.entity);
    visitor.visit();
  }

}
