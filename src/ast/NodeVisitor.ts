export interface NodeVisitor {
  visit(): void;
  getBodyNode(): any;
}
