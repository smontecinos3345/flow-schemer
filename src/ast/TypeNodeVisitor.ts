import { NodeVisitorFactory } from "./NodeVisitorFactory";
import { SchemaBodyNodeVisitor } from "./SchemaBody";

export class TypeNodeVisitor extends SchemaBodyNodeVisitor {
  constructor(factory: NodeVisitorFactory, context: any, ast: any) {
    super("objectvalue", factory, context, ast);
  }

  public getBodyNode() {
    return this.ast.right;
  }
}
