import { PropertySchema, TypeSchema, PrimitiveTypeSchema, Schema, EntitySchema, SchemaDict, EntityTypeSchema } from "../schema";
import { merge, lcfirst } from "../utils";

export * from "./joi";
export * from "./sequelize";

export function* propertyIterator(entitySchema: EntitySchema, properties: SchemaDict, nested: boolean)
  : IterableIterator<[string, Schema]> {

  for (const val of Object.entries(properties)) {
    const prop = val[1] as PropertySchema;
    const meta = merge((val[1] as PropertySchema).getMetadata());
    const type = prop.getType() as TypeSchema;
    const owned = !!meta.owned;
    const isKey = !!(meta.key);
    if (!type.isPrimitive()) {
      // composite keys should be owned
      if (!owned) {
        let type = (val[1] as PropertySchema).getType() as EntityTypeSchema;
        let nullable = type.isNullable();
        let props = type.wrapped().getChildSchemas();
        for (const key of Object.keys(props)) {
          const value = props[key] as PropertySchema;
          const isKey = !!merge(value.getMetadata()).key;
          if (isKey) {
            const name = `${val[0]}_${key}`.replace(/_+/g, "_");
            const type = (nullable ?
              (value.getType() as TypeSchema).asNullable() : value.getType());
            yield [name, new PropertySchema(name, type, value.getMetadata())];
          }
        }
        // yield [name, property as Schema];
        continue;
      }
    }

    // TODO: implement better strategies for handling nested objects
    if (isKey && nested) {
      const name = val[0];
      const propertyType = type.asNullable();
      yield [name, new PropertySchema(name, propertyType, prop.getMetadata())];
      continue;
    }

    if (!type.isPrimitive() && nested) {
      continue;
    }
    yield val;
  }

}
