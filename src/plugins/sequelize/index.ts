import { SchemaBuilder } from "../../schema";

export function sequelize(options: any) {

  return (builder: SchemaBuilder) => (f: any) => {
    const schemaName = f.path.split("/").slice(-1)[0].replace(/\.[^/.]+$/, "");
    f.path = f.path.replace(/\.(.+)$/, ".sequelize.$1");
    f.contents = Buffer.from(``);
    return f;
  };
}
