import { prettyPrint, IDict } from "../../utils";
import { JoiInterface } from "./JoiInterface";

function printArg(x?: any[], indent = 0) {
  return (x || []).map((y) => prettyPrint(y, indent)).join(",");
}

export function constructJoiBuilder(joiInterface: JoiInterface) {
  return (): any => {
    const res = new Proxy({
      isNullable: undefined,
      options: [],
      optional() {
        this.isNullable = true;
        return this;
      },
      required() {
        this.isNullable = false;
        return this;
      },
      prettyPrint(indent = 0) {
        if (this.isNullable === true) {
          this.options.push({ name: "optional" });
        } else if (this.isNullable === false) {
          this.options.push({ name: "required" });
        }
        return `${joiInterface.prefix}.` + this.options.map((x: any) =>
          `${x.name}(${printArg(x.args, indent)})`).join(".");
      },
      push(name: string, ...args: any[]) {
        this.options.push({ name, args });
        return res;
      },
    }, {
        get(target: any, prop: string) {
          if (prop in target) {
            return (...args: any[]) => {

              const fn = target[prop] as (...args: any[]) => any;
              return fn.apply(target, args);
            };
          }
          return (...args: any[]) => {
            return joiInterface.invokeMethod(res, prop, ...args);
          };
        },
      },
    );
    return res;
  };
}
