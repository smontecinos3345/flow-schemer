export interface JoiInterface {
  prefix: string;
  invokeMethod(joi: any, name: string, ...args: any[]): any;
}
