export default {
  keys(this: any, ...args: any[]) {
    return this.push("shape", args);
  },
  items(this: any, ...args: any[]) {
    return this.push("of", args);
  },
  match(this: any, ...args: any[]) {
    return this.push("matches", args);
  },
  prefix: "yup",
};
