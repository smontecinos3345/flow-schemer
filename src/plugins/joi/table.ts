import { PrimitiveTypeSchema } from "../../schema";
export default {
  [PrimitiveTypeSchema.date() as any]: "date",
  [PrimitiveTypeSchema.number() as any]: "number",
  [PrimitiveTypeSchema.string() as any]: "string",
  [PrimitiveTypeSchema.boolean() as any]: "boolean",
};
