import { PropertySchema } from "../../schema";
import { ArrayTypeSchema, EntityTypeSchema, TypeSchema } from "../../schema";
import { SchemaBuilder } from "../../schema";
import { EntitySchema } from "../../schema";
import { constructJoiBuilder } from "./joi";
import primitives from "./table";
import { merge, prettyPrint } from "../../utils";
import yup_properties from "./yup";
import { JoiInterface } from "./JoiInterface";
import deepmerge = require("deepmerge");
import common from "./common";
import { propertyIterator } from "../index";
const yup = deepmerge(common, yup_properties);

export function joiBrowser(joiInterface?: JoiInterface) {
  // @ts-ignore
  joiInterface = joiInterface ? deepmerge(yup, joiInterface) : yup;
  return (builder: SchemaBuilder) => joiPlugin(builder, joiInterface);
}

export function joiServer(joiInterface?: JoiInterface) {
  // @ts-ignore
  joiInterface = joiInterface ? deepmerge(common, joiInterface) : common;
  return (builder: SchemaBuilder) => joiPlugin(builder, joiInterface);
}

function joiPlugin(builder: SchemaBuilder, table: any) {

  const createJoi = constructJoiBuilder(table);

  return (f: any) => {
    const schemaName = f.path.split("/").slice(-1)[0].replace(/\.[^/.]+$/, "");
    const schema = builder.getSchema(schemaName) as EntitySchema;
    const res = buildSchema(schema);
    const importString = constructImport(table.prefix);
    const schemaString = `const keys = ${prettyPrint(res)};\nexport { keys };\n`;
    f.contents = Buffer.from(schemaString);
    return f;
  };

  function constructImport(prefix: string) {
    return `import * as ${prefix} from "${prefix}";`;
  }

  function buildSchema(schema: EntitySchema, nested = false) {
    const properties = schema.getChildSchemas();
    const res: any = {};

    for (const [name, prop] of propertyIterator(schema, properties, nested)) {
      const property = prop as PropertySchema;
      const meta = merge(property.getMetadata());
      res[name] = buildProperty(name, prop as PropertySchema, meta);
    }

    return res;
  }

  function buildProperty(name: string, prop: PropertySchema, meta: any) {
    let joi = createJoi() as any;
    const type = prop.getType() as TypeSchema;
    const { property } = meta;

    if (type.isPrimitive()) {
      joi = joi[primitives[type.wrapped() as any]]();
    } else if (type instanceof ArrayTypeSchema) {
      const inner = type.inner().wrapped();
      if (inner instanceof EntitySchema) {
        return createJoi().array().items(
          createJoi().object().keys(buildSchema(inner, true)),
        ).optional();
      } else {
        throw new Error("Not implemented yet!");
      }
    } else if (type instanceof EntityTypeSchema) {
      return createJoi().object().keys(buildSchema(type.wrapped() as EntitySchema, true));
    }

    if (property) {
      for (const [key, value] of Object.entries(property)) {
        // @ts-ignore
        joi = joi[key](value);
      }
    }

    if (type.isNullable()) {
      joi.optional();
    } else {
      joi.required();
    }
    return joi;
  }
}
