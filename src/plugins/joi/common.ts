export default {
  prefix: "joi",
  number: "number",
  string: "string",
  email: "email",
  date: "date",
  positive: "positive",
  array: "array",
  object: "object",
  invokeMethod(joi: any, name: string, ...args: any[]) {
    const val = (this as any)[name];
    if (name in this) {
      if (typeof val === "string") {
        const res = joi.push(val, args);
        return res;
      } else {
        return (this as any)[name].apply(joi, args);
      }
    }
    return joi;
  },
  range(this: any, arg: { min?: number, max?: number }) {
    if (arg) {
      const { min, max } = arg;
      if (min) {
        this.push("min", min);
      }
      if (max) {
        this.push("max", max);
      }
    }
    return this;
  },
  match(this: any, r: RegExp) {
    return this.push("regex", r);
  },
  subtype(this: any, name: string) {
    return this.push(name);
  },
  keys(this: any, ...args: any[]) {
    return this.push("keys", ...args);
  },
  items(this: any, ...args: any[]) {
    return this.push("items", ...args);
  },
  guid(this: any, ...args: any[]) {
    return this.push("guid", ...args);
  },
};
