import babylon = require("babylon");
import PluginError from "plugin-error";
import through2 = require("through2");
import { NodeVisitorFactory } from "./ast";
import { SchemaBuilder } from "./schema";

const pluginName = "gulp-flow-schemer";
import gulp = require("gulp");
import * as File from "vinyl";
import { SrcOptions } from "vinyl-fs";

// @ts-ignore
import gulpCollect = require("gulp-collect");

type FlowSchemerPlugin = (sch: SchemaBuilder) => (f: File) => any;

// tslint:disable-next-line: interface-name
export interface FlowSchemerOptions {
  plugins: FlowSchemerPlugin[];
  symbols: any;
  nameTransform?: (orig: string) => string;
}

export import plugins = require("./plugins");
export function flowSchemer(options: FlowSchemerOptions) {

  const symbols = options.symbols || { };
  const context = new SchemaBuilder(symbols);
  const factory = new NodeVisitorFactory();

  return {
    src(globs: string | string[], opt?: SrcOptions | undefined): any {
      return gulp
        .src(globs)
        .pipe(this.loadSchemas())
        // TODO: find out if the callback is necessary
        .pipe(gulpCollect.list((files: File[]) => {
          return files;
        }))
        // pipe through plugins
        .pipe(this.plugins());
    },
    loadSchemas() {
      return through2.obj((file, enc, cb) => {

        if (file.isNull()) {
          return cb(null, file);
        }

        if (file.isStream()) {
          return cb(new PluginError(pluginName, "Streaming not supported"));
        }

        const source = file.contents.toString();
        const ast = babylon.parse(source, {
          plugins: ["jsx", "flow"],
          sourceFilename: file.path,
          sourceType: "module",
        });

        const visitor =
          factory.getNodeVisitor(NodeVisitorFactory.INITIAL_NODE_VISITOR, ast, context);
        visitor.visit();
        const res = file.clone();
        if (options.nameTransform) {
          let n =  options.nameTransform(res.path);
          res.path = n;
        }
        res.contents = Buffer.from("");
        cb(null, res);
      });
    },
    plugins() {
      const pluginList = options.plugins.map((p) => p(context));
      return through2.obj(function(file, enc, cb) {
        try {
          pluginList.map((f) => f(file.clone())).forEach((f) => this.push(f));
          cb();
        } catch (err) {
          cb(err);
        }
      });
    },
  };
}
