import { Schema } from "./Schema";

export class PropertySchema implements Schema {
  constructor(private name: string, private type: Schema, private metadata: any[]) { }

  public getMetadata() {
    return this.metadata;
  }

  public getChildSchemas() {
    return {};
  }

  public getType(): Schema {
    return this.type;
  }
}
