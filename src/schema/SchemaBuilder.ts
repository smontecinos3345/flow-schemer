import { EntitySchema } from "./EntitySchema";
import { Schema } from "./Schema";

export class SchemaBuilder {

  private schemas: { [name: string]: Schema } = {};

  public constructor(public symbols?: any) { }

  public getSchema(name: string) {
    return this.schemas[name];
  }

  public createEntity(entity: string, metadata: any[], parents: string[]): any {
    this.schemas[entity] = new EntitySchema(entity, metadata, this, parents);
    return this.schemas[entity];
  }

}
