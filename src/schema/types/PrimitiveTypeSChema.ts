import { PrimitiveType } from ".";
import { TypeSchemaBase } from "./TypeSchemaBase";

export class PrimitiveTypeSchema extends TypeSchemaBase {

  public static number() {
    return PrimitiveTypeSchema._number;
  }

  public static string() {
    return PrimitiveTypeSchema._string;
  }

  public static date() {
    return PrimitiveTypeSchema._date;
  }

  public static boolean() {
    return PrimitiveTypeSchema._boolean;
  }

  // tslint:disable-next-line: variable-name
  private static readonly _number = new PrimitiveTypeSchema(Number);
  // tslint:disable-next-line: variable-name
  private static readonly _string = new PrimitiveTypeSchema(String);
  // tslint:disable-next-line: variable-name
  private static readonly _boolean = new PrimitiveTypeSchema(Boolean);
  // tslint:disable-next-line: variable-name
  private static readonly _date = new PrimitiveTypeSchema(Date);

  private constructor(base: PrimitiveType) { super(base); }

  public isEntity() {
    return false;
  }

  public isNullable() {
    return false;
  }

  public isPrimitive() {
    return true;
  }

  public getChildSchemas() {
    return {};
  }

  public toString() {
    return this.base.toString();
  }

}
