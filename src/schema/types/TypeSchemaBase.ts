import { PrimitiveType } from ".";
import { Schema, SchemaDict } from "../Schema";
import { NullableTypeSchema } from "./NullableTypeSchema";
import { TypeSchema } from "./TypeSchema";

export abstract class TypeSchemaBase implements TypeSchema {

  constructor(protected base: any) { }

  public abstract isNullable(): boolean;

  public asNullable(): TypeSchema {
    return new NullableTypeSchema(this);
  }

  public abstract isEntity(): boolean;

  public wrapped(): Schema | PrimitiveType {
    return this.base;
  }

  public isGeneric() {
    return false;
  }

  public isPrimitive() {
    return false;
  }

  public abstract getChildSchemas(): SchemaDict;

}
