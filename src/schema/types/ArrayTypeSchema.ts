
import { SchemaDict } from "../Schema";
import { TypeSchema } from "./TypeSchema";
import { TypeSchemaBase } from "./TypeSchemaBase";

export class ArrayTypeSchema extends TypeSchemaBase {

  // tslint:disable-next-line: variable-name
  constructor(private _inner: TypeSchema) { super(Array); }

  public isNullable(): boolean {
    return false;
  }

  public inner() {
    return this._inner;
  }

  public getChildSchemas(): SchemaDict {
    return {};
  }

  public isEntity() {
    return false;
  }

}
