import { TypeSchema } from "./TypeSchema";

export class NullableTypeSchema implements TypeSchema {

  // tslint:disable-next-line: variable-name
  constructor(private _wrapped: TypeSchema) { }

  public isNullable() {
    return true;
  }

  public wrapped() {
    return this._wrapped.wrapped();
  }

  public asNullable() {
    return this;
  }

  public isEntity() {
    return this._wrapped.isEntity();
  }

  public isPrimitive() {
    return this._wrapped.isPrimitive();
  }

  public isGeneric() {
    return true;
  }

  public getChildSchemas() {
    return this._wrapped.getChildSchemas();
  }

}
