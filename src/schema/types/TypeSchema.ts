import { PrimitiveType } from ".";
import { Schema } from "../Schema";

export interface TypeSchema extends Schema {

  isNullable(): boolean;
  asNullable(): TypeSchema;
  isEntity(): boolean;
  isPrimitive(): boolean;
  wrapped(): Schema | PrimitiveType | undefined;
  isGeneric(): boolean;

}
