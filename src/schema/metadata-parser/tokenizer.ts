export const PUNCTUATOR = "PUNCTUATOR";
export const ISTAR = "ISTAR";
export const NUMBER = "NUMBER";
export const BOOLEAN = "BOOL";
export const NULL = "NULL";
export const IDENT = "IDENT";
export const START = "START";
export const WHITE = "END";
export const STRING = "STRING";
export const UNKOWN = "UNKOWN";
export const REGEXP = "REGEXP";
export const EOF = "EOF";

// tslint:disable-next-line: interface-name
export interface Token {
  type: string;
  lexem: string;
}

const tokenizer = (() => {

  // TODO: make sure ISTAR ocurs only at the beginning of line
  const tokens = [
    { tokenType: PUNCTUATOR, rex: /[\[\]{}(),:]/ },
    { tokenType: START, rex: /@[A-Za-z_$][A-Z-a-z0-9_$]*/ },
    { tokenType: ISTAR, rex: /\*/ },
    { tokenType: NUMBER, rex: /-?(?:0|[1-9]\d*)(?:\.\d+)?(?:[eE][+-]?\d+)?/ },
    { tokenType: BOOLEAN, rex: /true|false/ },
    { tokenType: NULL, rex: /null/ },
    { tokenType: IDENT, rex: /[A-Za-z_$][A-Z-a-z0-9_$]*/ },
    { tokenType: STRING, rex: /("([^"]|\\")*")|('([^']|\\')*')/ },
    { tokenType: WHITE, rex: /\s+/ },
    { tokenType: REGEXP, rex: /\/(\\.|[^/])+\/[gim]*/ },
    { tokenType: UNKOWN, rex: /.+/ },
  ];

  const tokenTable: { [x: string]: string } = {};    // table to get token type

  function addRange(a: string, b: string, type: string) {

    for (let i = a.charCodeAt(0), l = b.charCodeAt(0); i <= l; i++) {
      tokenTable[String.fromCodePoint(i)] = type;
    }
  }

  function addSequence(str: string, type: string) {
    str.split(new RegExp("")).forEach((x) => tokenTable[x] = type);
  }

  addSequence("()[]{},:", PUNCTUATOR);
  addRange("A", "Z", IDENT);
  addRange("a", "z", IDENT);
  addSequence("$_", IDENT);
  addSequence("@", START);
  addSequence("*", ISTAR);
  addSequence(`"'`, STRING);
  addRange("0", "9", NUMBER);
  addSequence("-.", NUMBER);
  addSequence("/", REGEXP);

  tokenTable.true = BOOLEAN;
  tokenTable.false = BOOLEAN;
  tokenTable.null = NULL;

  return {
    *tokenize(str: string) {
      const regex = new RegExp(tokens.map((x) => `(${x.rex.source})`).join("|"), "gm");
      let m: RegExpExecArray | null;

      // tslint:disable-next-line: no-conditional-assignment
      while ((m = regex.exec(str)) !== null) {
        // This is necessary to avoid infinite loops with zero-width matches
        if (m.index === regex.lastIndex) {
          regex.lastIndex++;
        }
        const lexem = m[0].trim();
        if (lexem === "") {
          continue;
        }
        yield { type: tokenTable[lexem] || tokenTable[lexem[0]] || UNKOWN, lexem } as Token;
      }
      yield { type: EOF, lexem: "" };
    },
  };
})();

export default tokenizer;
