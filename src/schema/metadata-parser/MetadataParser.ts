import { StringBuffer } from "./parser";
import { Token } from "./tokenizer";
import * as tok from "./tokenizer";
const { BOOLEAN } = tok;
const { EOF } = tok;
const { IDENT } = tok;
const { ISTAR } = tok;
const { NULL } = tok;
const { NUMBER } = tok;
const { PUNCTUATOR } = tok;
const { REGEXP } = tok;
const { START } = tok;
const { STRING } = tok;
type Result<T> = T | Error;
export type Type<T> = Result<T>;

export function isError<T>(result: Result<T>): result is Error {
  return result instanceof Error;
}
export class MetadataParser {

  private lastToken: Token | null = null;

  constructor(protected it: IterableIterator<Token>, private defs = {}) { }

  public parse(): any {
    const data: any = {};
    let token: Token;
    while ((token = this.next()).type !== EOF) {
      if (token.type === START) {
        const e: Result<any> = this.convertToJSON();
        if (!isError(e)) {
          data[token.lexem.substr(1)] = e;
        } else {
          // tslint:disable-next-line: no-console
          console.error(e);
        }
      }
    }
    return data;
  }

  public saveToken(token: Token) {
    this.lastToken = token;
  }

  public next() {
    let obj: { done: boolean, value: Token };
    if (this.lastToken !== null) {
      const res = this.lastToken;
      this.lastToken = null;
      return res;
    }

    // tslint:disable-next-line: no-conditional-assignment
    while ((obj = this.it.next()), !obj.done) {
      if (obj.value.type !== ISTAR) {
        break;
      }
    }
    if (obj.done) {
      return { type: EOF, lexem: "" };
    }
    return obj.value;
  }

  public convertToJSON(): Result<any> {
    try {
      const buf = StringBuffer.from("");
      this.expectString("(");
      this.getValue(buf);
      this.expectString(")");
      const env = this.defs || {};
      // tslint:disable-next-line: no-eval
      return eval(this.getEvaluationString(buf.toString())); // THE HORROOOOR!
    } catch (err) {
      return err;
    }
  }

  public getValue(buf: StringBuffer) {
    const token = this.next();
    if (token.lexem === "[") {
      this.array(buf);
    } else if (token.lexem === "{") {
      this.object(buf);
    } else if (token.type === NUMBER) {
      buf.write(token.lexem);
    } else if (token.type === BOOLEAN) {
      buf.write(token.lexem);
    } else if (token.type === NULL) {
      buf.write(token.lexem);
    } else if (token.type === STRING) {
      buf.write(token.lexem);
    } else if (token.type === REGEXP) {
      buf.write(token.lexem);
    } else if (token.type === IDENT) {
      buf.write(`env.${token.lexem}`);
    } else {
      throw new Error(`Syntax Error: ${token.lexem}`);
    }
  }

  public array(buf: StringBuffer) {
    buf.write("[");
    this.elements(buf);
    this.consumeOrSave("]", buf);
  }

  public elements(buf: StringBuffer) {
    if (this.consumeOrSave("]", buf).didSave) {
      this.getValue(buf);
      if (this.consumeOrSave(",", buf).didConsume) {
        this.elements(buf);
      }
    }
  }

  public object(buf: StringBuffer) {
    buf.write("{");
    this.pairs(buf);
    this.consumeOrSave("}", buf);
  }

  public pairs(buf: StringBuffer) {
    let existentialProperty = false; // property value is "property exists"
    if (this.consumeOrSave("}", buf).didSave) {
      const next = this.next();
      if (next.type !== IDENT && next.type !== STRING) {
        throw new Error("Syntax error: expecting object key");
      }
      buf.write(next.lexem);
      if (this.consumeOrSave(":", buf).didSave) {
        if (this.consumeOrSave(",", StringBuffer.from("")).didConsume) {
          buf.write(": __obj__,");
          existentialProperty = true;
        } else {
          throw new Error("Syntax error: expecting object value");
        }
      }
      if (!existentialProperty) {
        this.getValue(buf);
      }
      if (existentialProperty || this.consumeOrSave(",", buf).didConsume) {
        this.pairs(buf);
      }
    }
  }

  public consumeOrSave(str: string, buf: StringBuffer) {
    const token = this.next();
    if (token.lexem === str) {
      buf.write(token.lexem);
      return { didConsume: true, didSave: false };
    } else {
      this.saveToken(token);
      return { didConsume: false, didSave: true };
    }
  }

  public consume(str: string, buf: Buffer, message?: string) {
    let offendingString;
    // tslint:disable-next-line: no-conditional-assignment
    if ((offendingString = this.next().lexem) !== str) {
      const errMsg = (
        message || `Syntax error: expecting ${str} but got ${offendingString}`
      );
      throw Error(errMsg);
    }
    buf.write(str);
  }

  public expectString(str: string, message?: string) {
    let offendingString;
    // tslint:disable-next-line: no-conditional-assignment
    if ((offendingString = this.next().lexem) !== str) {
      const errMsg = (
        message || `Syntax error: expecting ${str} but got ${offendingString}`
      );
      throw Error(errMsg);
    }
  }

  private getEvaluationString(str: string) {
    return `; (function (__obj__) {
      return ${str};
    }(Object.freeze({ prettyPrint() { return ""; } })))`;
  }

}
