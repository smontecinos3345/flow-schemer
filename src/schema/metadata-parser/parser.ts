// FIXME: use of buffer
export class StringBuffer {

  public static from(str: string) {
    return new StringBuffer(str);
  }

  private constructor(private value: string) { }

  public write(str: string) {
    this.value += str;
  }

  public toString() {
    return this.value;
  }

}
