import { SchemaBase } from ".";
import { EntityTypeSchema } from "./EntityTypeSchema";
import { PropertySchema } from "./PropertySchema";
import { Schema, SchemaDict } from "./Schema";
import { TypeSchema } from "./types";

export class EntitySchema extends SchemaBase implements EntitySchema {

  private properties: SchemaDict = { };

  public getChildSchemas() {
    let res = {};
    const parents = this.parents.map(this.createSchemaReference);
    for (const parent of parents) {
      res = { ...res, ...parent.getChildSchemas() };
    }
    return { ...res, ...this.properties };
  }

  public getOwnChildSchemas() {
    return { ...this.properties };
  }

  public addProperty(name: string, type: Schema, metadata: any[]) {
    this.properties[name] = new PropertySchema(name, type, metadata);
    return this.properties[name];
  }

  public asTypeSchema(): TypeSchema {
    return new EntityTypeSchema(this.name, this.builder);
  }

  public createSchemaReference = (name: string) => {
    return new EntityTypeSchema(name, this.builder);
  }

}
