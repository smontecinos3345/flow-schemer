import { SchemaBuilder } from "./SchemaBuilder";
import { TypeSchema } from "./types";
import { NullableTypeSchema } from "./types";

export class EntityTypeSchema implements TypeSchema {

  constructor(private name: string, protected builder: SchemaBuilder) { }

  public isNullable() {
    return false;
  }

  public wrapped() {
    return this.builder.getSchema(this.name);
  }

  public asNullable(): TypeSchema {
    return new NullableTypeSchema(this);
  }

  public isPrimitive() {
    return false;
  }

  public isGeneric() {
    return false;
  }

  public isEntity() {
    return true;
  }

  public getChildSchemas() {
    // tslint:disable-next-line: variable-name
    const wrapped_ = this.wrapped();
    if (wrapped_) {
      return wrapped_.getChildSchemas();
    }
    return {};
  }

}
