export interface Schema {
  getChildSchemas(): SchemaDict;
}

export interface SchemaDict { [name: string]: Schema; }
