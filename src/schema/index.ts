export * from "./metadata-parser";
export * from "./Schema";
export * from "./types";
import { Schema, SchemaDict } from "./Schema";
import { SchemaBuilder } from "./SchemaBuilder";

export interface Entity extends Schema {
  addProperty(name: string, type: Schema, metadata: any[]): void;
}

export abstract class SchemaBase implements Schema {

  constructor(protected name: string, protected metadata: any[], protected builder: SchemaBuilder, protected parents: string[]) { }

  public getMetadata() {
    return this.metadata;
  }

  public getBuilder() {
    return this.builder;
  }

  public abstract getChildSchemas(): SchemaDict;
}

export * from "./Schema";
export * from "./SchemaBuilder";
export * from "./EntitySchema";
export * from "./EntityTypeSchema";
export * from "./PropertySchema";
