// @flow
// @ts-ignore
import { Order } from "./Order";
import { Product } from "./Product";

export interface OrderDetail {
	// @key(true)
	order: Order;
	// @key(true)
	product: Product;
	quantity: number;
	price: number;
	discount: number;
	iva: number;
}
