import { Entity } from "./Entity";

// @flow

export interface Category extends Entity {
	name: string;
	desc: string;
}
