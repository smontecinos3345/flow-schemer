// @flow

import { Money } from "./Money";
import { Entity } from "./Entity";

export interface Position extends Entity{

	name: string;

	desc: string;

	// @owned(true)
	salary: Money;

}
