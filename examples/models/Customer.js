
// @flow

import { Person } from './Person';
import { Order } from './Order';
export interface Customer extends Person{

	// @owned(true)
	orders: Array<Order>;
}
