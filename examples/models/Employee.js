// @flow
import { Person } from "./Person";
import { Position} from "./Position";

export interface Employee extends Person {
	manager?: Employee;

	position: Position;
}
