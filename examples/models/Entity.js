// @flow

export interface Entity {

	// @key(true)
	/**
	 * @property({
		guid: [{ version : 'uuidv4' }],
	})*/
	_id: string;

	_rev?: string;

	type: string;

}
