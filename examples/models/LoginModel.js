// @flow

export interface LoginModel {
	/**
		@property({
		  subtype: "email",
		})
	*/
	email: string;

	/**
	 * @property({
	 *     match: password_regex,
	 * })
	*/
	password: string;
}
