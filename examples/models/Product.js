// @flow
import { Category } from "./Category";
import { Entity } from "./Entity";
export interface Product extends Entity {

	price: number;

	name: string;

	desc: string;

	category: Category;

}
