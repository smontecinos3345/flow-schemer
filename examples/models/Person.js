import { Entity } from "./Entity";

/**
 * @model({
 *   sensible: ["discriminator"],
 * })
*/

export interface Person extends Entity{

	/**
	 * @property({
	 *   match: /^[^d]+$/,
	 * })
	*/

	firstName: string;

	/**
	 * @property({
	 *   match: /^[^d]+$/,
	 * })
	*/
	lastName: string;

	birthDate?: Date;

	ndi?: string;
}
