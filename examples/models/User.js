import { Entity } from "./Entity";

import { Role } from "./Role";

export interface User extends Entity {

	active: boolean;

	// @owned(true)
	roles: Array<Role>;
}
