// @flow

import { OrderDetail } from "./OrderDetail";
import { Employee } from "./Employee";
import { Customer } from "./Customer";
import { Entity } from "./Entity";
export interface Order extends Entity {

	employee: Employee;

	customer: Customer;

	orderDate: Date;

	// @owned(true)
	details: Array<OrderDetail>;
}
