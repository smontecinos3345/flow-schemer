const gulp = require('gulp');
const { flowSchemer, plugins } = require('../lib');

const password_regex = /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/;

let schemer = flowSchemer({
  plugins: [plugins.joiServer()],
	symbols: {
		password_regex,
	}
});

let schemerUI = flowSchemer({
  plugins: [plugins.joiBrowser()],
  symbols: {
    password_regex,
  },
});

gulp.task('generate-backend', function () {
  return schemerUI.src('./models/**/*.js')
    .pipe(gulp.dest('./ui'));
});

gulp.task('generate-frontend', function() {
  return schemer.src('./models/**/*.js')
    .pipe(gulp.dest('./api'));
});

gulp.task('default', gulp.series(['generate-frontend', 'generate-backend']));
